# admin-system

> Vue全家桶实现通用后台管理系统，可二次开发，喜欢的给个star哦！

## Build Setup

``` bash
# 安装依赖
npm install

# 运行
npm run dev

# 打包
npm run build

# 打包
npm run build --report

# 运行单元测试
npm run unit

# 运行e2e测试
npm run e2e

# 运行全部测试
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
